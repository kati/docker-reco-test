#!/bin/bash

# exit when any command fails; be verbose
set -ex

# make cmsrel etc. work
shopt -s expand_aliases
export MY_BUILD_DIR=${PWD}
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd /home/cmsusr
cmsrel CMSSW_5_3_32
mkdir -p CMSSW_5_3_32/src/AnalysisCode
mv ${MY_BUILD_DIR}/RawToRecoTest CMSSW_5_3_32/src/AnalysisCode
cd CMSSW_5_3_32/src
cmsenv
ls /cvmfs/cms-opendata-conddb.cern.ch
cmsRun AnalysisCode/RawToRecoTest/rawtoreco_config.py